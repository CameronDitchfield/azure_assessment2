#!/bin/bash -xv

if (( $# < 4 ))
then
  echo "usage: $0 <database_ip> <database_name> <database_user> <database_password> <database_servername>" 1>&2
  exit 1
else 
  dbip=$1
  dbname=$2
  dbuser=$3
  dbpass=$4
  dbservername=$5
fi

#Installations
yum -y install java-1.8.0-openjdk

mkdir -p /opt/petclinic/config
wget https://ass2storage.blob.core.windows.net/petclinic/spring-petclinic-2.0.0.jar
cp *.jar /opt/petclinic/
mv /opt/petclinic/*.jar /opt/petclinic/petclinic.jar

cat > /opt/petclinic/config/application.properties <<_END_
database=mysql
spring.datasource.url=jdbc:mysql://${dbip}/${dbname}
spring.datasource.username=${dbuser}@${dbservername}
spring.datasource.password=${dbpass}
spring.jpa.hibernate.ddl-auto=none
logging.level.org.springframework=INFO
spring.profiles.active=production
_END_

cat > /etc/init.d/pc << '_END_'
#!/bin/bash
#description: petclinic control
#chkconfig: 2345 99 99
case $1 in
    start)
        cd /opt/petclinic
        nohup java -jar petclinic.jar > /var/log/petclinic.log 2>&1 &
        ;;
    stop)
        kill $(ps -ef | grep petclinic | grep -v grep | awk '{print $2}')
        ;;
    status)
        ps -ef | grep petclinic | grep -v grep | awk '{print $2}'
        ;;
esac
_END_

chmod +x /etc/init.d/pc
chkconfig --add pc
service pc start