#!/bin/bash -xv

RGRP_NAME=assessment2_group
SSH_KEY=~/.ssh/azure.pub
DB_NAME=petclinic
DB_USER=petclinic
DB_PASS=magicAl21!
IP=81.100.212.63
LOC=westeurope
APP_PREFIX=pc
DB_SERVER_NAME=${APP_PREFIX}-db-cameron-al

# Create resource group
az group create --name $RGRP_NAME --location $LOC


# Networking ======================================================================================
# Create VNet
az network vnet create --resource-group $RGRP_NAME --name ${APP_PREFIX}vnet --address-prefixes 10.1.0.0/16 --subnet-name ${APP_PREFIX}backendsnet --subnet-prefixes 10.1.0.0/24 --location $LOC

# Create public IP and subnet for bastion
az network public-ip create --resource-group $RGRP_NAME --name ${APP_PREFIX}bastionip --sku Standard
az network vnet subnet create --resource-group $RGRP_NAME --vnet-name ${APP_PREFIX}vnet --name AzureBastionSubnet --address-prefixes 10.1.1.0/24

# Create bastion
az network bastion create --resource-group $RGRP_NAME --location $LOC --name ${APP_PREFIX}bastion --public-ip-address ${APP_PREFIX}bastionip --vnet-name ${APP_PREFIX}vnet

# Create nsg and add rules
az network nsg create --resource-group $RGRP_NAME --name ${APP_PREFIX}nsg
az network nsg rule create --resource-group $RGRP_NAME --nsg-name ${APP_PREFIX}nsg --name ${APP_PREFIX}rule --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 8080 --access allow --priority 200

# Create nic for petclinic VM
az network nic create --resource-group $RGRP_NAME --name ${APP_PREFIX}nic --vnet-name ${APP_PREFIX}vnet --subnet ${APP_PREFIX}backendsnet --network-security-group ${APP_PREFIX}nsg

# Public IP for petclinic
az network public-ip create --resource-group $RGRP_NAME --name ${APP_PREFIX}ip --sku Standard


# MYSQL SERVER ====================================================================================
# Create mysql server
server_info=$(az mysql server create --name $DB_SERVER_NAME --resource-group $RGRP_NAME --location $LOC --admin-user $DB_USER --admin-password $DB_PASS --sku-name B_Gen5_1 --ssl-enforcement Disabled)

# Create firewall rules for IP and internal requests on mysql server
az mysql server firewall-rule create --resource-group $RGRP_NAME --server $DB_SERVER_NAME --name home_ip --start-ip-address $IP --end-ip-address $IP
az mysql server firewall-rule create --resource-group $RGRP_NAME --server $DB_SERVER_NAME --name allow_internal --start-ip-address 0.0.0.0 --end-ip-address 255.255.255.255

# Get the mysql server host name from output json
DB_IP=$(echo $server_info | jq -r '.fullyQualifiedDomainName')

# Execute the petclinic sql scripts on the mysql server
mysql -h $DB_IP -u $DB_USER@$DB_SERVER_NAME -p$DB_PASS < petclinic/schema.sql
mysql -h $DB_IP -u $DB_USER@$DB_SERVER_NAME -p$DB_PASS < petclinic/data.sql


# Petclinic VM ====================================================================================
# Create the VM for petclinic
az vm create --verbose --image CentOS --ssh-key-values $SSH_KEY --resource-group $RGRP_NAME --name ${APP_PREFIX}vm --location $LOC --admin-username cameron --nics ${APP_PREFIX}nic

# Use CustomScript extension to install petclinic.
az vm extension set \
    --publisher Microsoft.Azure.Extensions \
    --version 2.0 \
    --name CustomScript \
    --vm-name ${APP_PREFIX}vm \
    --resource-group $RGRP_NAME \
    --settings '{"fileUris":["https://bitbucket.org/CameronDitchfield/azure_assessment2/raw/dcdccf8be00ffc437d06bef9ad3179d3ad6b9cb5/petclinic/install_pc.sh"], "commandToExecute":"sh install_pc.sh '$DB_IP' '$DB_NAME' '$DB_USER' '$DB_PASS' '$DB_SERVER_NAME'"}'

# Loadbalancer ====================================================================================
# Create the lb
az network lb create --resource-group $RGRP_NAME --name ${APP_PREFIX}lb --sku Standard --public-ip-address ${APP_PREFIX}ip --frontend-ip-name ${APP_PREFIX}frontend --backend-pool-name ${APP_PREFIX}backendpool

# lb health probe
az network lb probe create --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb --name ${APP_PREFIX}healthprobe --protocol tcp --port 8080

# lb rule/s
az network lb rule create --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb --name ${APP_PREFIX}lbrule --protocol tcp --frontend-port 80 --backend-port 8080 --frontend-ip-name ${APP_PREFIX}frontend --backend-pool-name ${APP_PREFIX}backendpool --probe-name ${APP_PREFIX}healthprobe --disable-outbound-snat true --idle-timeout 15 --enable-tcp-reset true

# Add VM/s to lb backend pool
az network nic ip-config address-pool add --address-pool ${APP_PREFIX}backendpool --ip-config-name ipconfig1 --nic-name ${APP_PREFIX}nic --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb

# outbound rule config ============================================================================
# IP for outbound conn
az network public-ip create --resource-group $RGRP_NAME --name ${APP_PREFIX}publicipoutbound --sku Standard
az network public-ip prefix create --resource-group $RGRP_NAME --name ${APP_PREFIX}publicipprefixoutbound --length 28

# Attach outbound conn IPs to lb
az network lb frontend-ip create --resource-group $RGRP_NAME --name ${APP_PREFIX}frontendoutbound --lb-name ${APP_PREFIX}lb --public-ip-address ${APP_PREFIX}publicipoutbound
az network lb address-pool create --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb --name ${APP_PREFIX}backendpooloutbound

# Outbound rule 
az network lb outbound-rule create --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb --name ${APP_PREFIX}outboundrule --frontend-ip-configs ${APP_PREFIX}frontendoutbound --protocol All --idle-timeout 15 --outbound-ports 10000 --address-pool ${APP_PREFIX}backendpooloutbound

# Add VM/s to outbound pool
az network nic ip-config address-pool add --address-pool ${APP_PREFIX}backendpooloutbound --ip-config-name ipconfig1 --nic-name ${APP_PREFIX}nic --resource-group $RGRP_NAME --lb-name ${APP_PREFIX}lb
